# SiNVICT #

SiNVICT is a tool for the detection of SNVs and indels from cfDNA/ctDNA samples obtained by ultra-deep sequencing.

### Prerequisites: ###
 * g++ version: 4.8.3
 * Boost version: 1_53

### Input Data: ###
 * SiNVICT requires one or more "readcount" files as input, which can be obtained from the tool bam-readcount.
    * See https://github.com/genome/bam-readcount for a detailed description of this file format.
    * bam-readcount is included as a submodule with SiNVICT
 * The detailed steps for obtaining these readcount files to be used by SiNVICT are explained below.

### Obtaining readcount files - Preparation of the samples for SiNVICT: ####
 * Trimming FASTQ files (optional; recommended), (fastq -> fastq)
    * Use one of the available FASTQ trimmers (such as http://hannonlab.cshl.edu/fastx_toolkit/) to remove any remaining adapters, barcodes, etc...
    * fastx_toolkit is included as a submodule with SiNVICT
 * Mapping (fastq -> bam)
    * Use one of the available short read mappers to map the reads (mrFAST, bwa, bowtie, etc...)
    * bwa is included as a submodule with SiNVICT
 * Recalibration and error correction (optional, recommended), (bam -> bam)
    * Use one of the available tools for this type of data processing (such as https://github.com/mozack/abra)
    * ABRA is included as a submodule with SiNVICT
 * Use bam-readcount to obtain mapping statistics per location, (bam -> readcount)
    * bam-readcount is included as a submodule with SiNVICT

### Getting SiNVICT: ###

SiNVICT can either be downloaded from the "Downloads" section of this repository directly or via git with the following command:

```
git clone https://bitbucket.org/ckockan_cs/sinvict.git
```

To get all submodules included with SiNVICT as well, the following command can be used:

```
git clone --recursive https://bitbucket.org/ckockan_cs/sinvict.git
```

### Running SiNVICT: ###

```
cd path_to_sinvict
./sinvict -e [errorRate] -m [minDepth] -l [leftStrandBias] -r [rightStrandBias] -f [readEndFraction] -q [qScoreCutoff] -t <tumourDirectoryPath> -o <outputDirectoryPath>
```

Using the the help option will print out the following:

```
./sinvict -h
```

```text
SiNVICT: Ultra Sensitive Detection of Single Nucleotide Variants and Indels in Circulating Tumour DNA.

Allowed arguments:
  -h [ --help ]                    Print help message.
  -e [ --errorRate ] arg           Error rate for the sequencing platform used.
  -m [ --minDepth ] arg            Minimum Read Depth required for high 
                                   confidence in a call.
  -l [ --leftStrandBias ] arg      Lower limit for the strand bias value 
                                   interval to be used in assessing the 
                                   confidence of a call.
  -r [ --rightStrandBias ] arg     Upper limit for the strand bias value 
                                   interval to be used in assessing the 
                                   confidence of a call.
  -f [ --readEndFraction ] arg     Average position of the called base on the 
                                   reads supporting the call as a fraction. End
                                   values such as 0.01 as useful for filtering 
                                   read end artifacts.
  -q [ --QScoreCutoff ] arg        Cutoff value for the qScore assigned to each
                                   call by the Poisson model used.
  -t [ --tumorDirectoryPath ] arg  Specifies directory for the input files.
  -o [ --outputDirectoryPath ] arg Specifies directory for the output files.

  --tumorDirectoryPath and --outputDirectoryPath must be specified

  Usage: ./sinvict -e [errorRate] -m [minDepth] -l [leftStrandBias] -r [rightStrandBias] -f [readEndFraction] -q [QScoreCutoff] -t <tumorDirectoryPath> -o <outputDirectoryPath>
```

### SiNVICT Input Parameters: ###
 * --errorRate: Error Rate for the sequencing technology used (e.g. Illumina, Ion Torrent, ...)
 * --minDepth: Minimum required read depth for a call to be considered reliable.
 * --leftStrandBias and --rightStrandBias: The strand bias values in the range [leftStrandBias, rightStrandBias] will be considered reliable. This [lsb, rsb] interval has to be between [0,1].
 * --readEndFraction: Despite the trimming step, calls can be marked as low confidence according to the average position of the base on the reads that support a call. This value should be within range 0-1.
 * --QScoreCutoff: The poisson model used by SiNVICT assigns a QScore to every call. Calls with a QScore below the user defined threshold will be considered low confidence. This value should be in range 0-99.
 * --tumorDirectoryPath: The path to the directory where the readcount files are located.
 * --outputDirectoryPath: The path to an empty directory where the output files will be generated.

--tumorDirectoryPath and --outputDirectoryPath parameters are required. All other parameters are optional, but it is highly recommended to set them.

The default parameters are given in the next subsection.

#### Default Parameters: ####
 * --errorRate=0.01
 * --minDepth=100
 * --leftStrandBias=0.3
 * --rightStrandBias=0.7
 * --readEndFraction=0.01
 * --QScoreCutoff=95

## Output ##
Output files are ordered according to their level of confidence/filtering (each layer filters some calls from the previous layer). The ordering of the files is given in the following subsection.

### Output Files: ###
 * Poisson model: calls_level1.sinvict
 * Minimum Read Depth filter: calls_level2.sinvict
 * Strand-bias filter: calls_level3.sinvict
 * Average position of called location among all reads supporting the call: calls_level4.sinvict
 * Signal-to-Noise ratio filter: calls_level5.sinvict
 * Homopolymer Regions filter: calls_level6.sinvict


Each output file contains a number of calls. Each line within a file corresponds to a call. Each line is tab delimited. The following is a sample output from SiNVICT:

```
chrX    66943552        22RV1_49C_10_to_1_10ng  A       3709    G       602     16.2308 +:430   -:172   0.53    Somatic
chrX    66943552        22RV1_49C_10_to_1_1ng   A       1979    G       111     5.60889 +:75    -:36    0.54    Somatic
chrX    66943552        22RV1_49C_10_to_1_2.5ng A       6221    G       803     12.9079 +:567   -:236   0.53    Somatic
chrX    66943552        22RV1_49C_10_to_1_5ng   A       2508    G       265     10.5662 +:183   -:82    0.52    Somatic
chrX    66943552        22RV1_49C_20_to_1_10ng  A       5247    G       304     5.79379 +:167   -:137   0.53    Somatic
chrX    66943552        22RV1_49C_20_to_1_1ng   A       3614    G       141     3.90149 +:83    -:58    0.52    Somatic
chrX    66943552        22RV1_49C_20_to_1_2.5ng A       7183    G       290     4.03731 +:174   -:116   0.52    Somatic
chrX    66943552        22RV1_49C_20_to_1_5ng   A       4969    G       272     5.47394 +:140   -:132   0.51    Somatic
chrX    66943552        22RV1_49C_50_to_1_10ng  A       7130    G       127     1.78121 +:71    -:56    0.53    Somatic
chrX    66943552        22RV1_49C_50_to_1_1ng   A       4006    G       113     2.82077 +:60    -:53    0.52    Somatic
chrX    66943552        22RV1_49C_50_to_1_2.5ng A       4646    G       142     3.05639 +:88    -:54    0.52    Somatic
chrX    66943552        22RV1_49C_50_to_1_5ng   A       2995    G       69      2.30384 +:46    -:23    0.54    Somatic
chrX    66943552        22RV1_49C_5_to_1_10ng   A       2917    G       681     23.3459 +:506   -:175   0.53    Somatic
chrX    66943552        22RV1_49C_5_to_1_1ng    A       2225    G       494     22.2022 +:347   -:147   0.53    Somatic
chrX    66943552        22RV1_49C_5_to_1_2.5ng  A       2743    G       737     26.8684 +:561   -:176   0.53    Somatic
chrX    66943552        22RV1_49C_5_to_1_5ng    A       3365    G       574     17.0579 +:480   -:94    0.52    Somatic
```

A more detailed explanation of what each field corresponds to is given in the next subsection (given in the same order they appear within files).

### Output Fields: ###
 * Chromosome Name
 * Position
 * Sample Name (readcount file name)
 * Reference Base
 * Read Depth
 * Mutated Base(s)
 * Number of reads supporting the mutation
 * Variant Allele Percentage
 * Number of reads mapped to the + strand (in format "+:`value`")
 * Number of reads mapped to the - strand (in format "-: `value`")
 * Average position of the base on reads as a fraction (0.00 - 1.00)
 * "Somatic" or "Germline", predicted based on variant allele frequency (do not take as the ground truth).

#### Contact ####
For any additional questions/comments/suggestions, please send an email to the following address:

ckockan@sfu.ca